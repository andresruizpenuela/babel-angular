import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogAnimationsExampleDialog } from './dialog-modal.component';
import "@angular/compiler";
import { Observable, Subject } from 'rxjs';
@Injectable({providedIn: 'root'})
export class DialogModalService {

  constructor( private dialog:MatDialog) { }


  // muestra un dialaogo de angular material con propieades
  showDialogModal(title:string,message:string):Observable<any>{
    const dialogRef = this.dialog.open(DialogAnimationsExampleDialog, {
      width: '250px',
      data: {title: title,message: message}
    });

    // para enviar la informaicón de la decisión
    // al componente que lo genero, se usa un observable
    const subjet = new Subject();


    dialogRef.afterClosed().subscribe(result=> {
      //console.log(result);
      subjet.next(result);
    });

    return subjet.asObservable();
  }
}
