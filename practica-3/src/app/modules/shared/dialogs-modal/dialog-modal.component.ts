import { Component, Inject} from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MatDialogActions,
  MatDialogClose,
  MatDialogTitle,
  MatDialogContent,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

//import {MatButtonModule} from '@angular/material/button';
/**
 * @title Dialog Animations
 */
@Component({
  selector: 'dialog-modal',
  templateUrl: 'dialog-modal.html'
})
export class DialogAnimationsExampleDialog {
  constructor(public dialogRef: MatDialogRef<DialogAnimationsExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data:any) {}
}
