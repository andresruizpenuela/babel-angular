import { CUSTOM_ELEMENTS_SCHEMA, NgModule  } from '@angular/core';

import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';

//import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { PasswordModule } from 'primeng/password';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { DialogAnimationsExampleDialog } from './dialogs-modal/dialog-modal.component';
import { DialogModalService } from './dialogs-modal/dialog-modal.service';
import { SpinnerService } from './spinner/spinner.service';

const angularMaterial = [
  MatTooltipModule,
  MatButtonModule,
  MatSnackBarModule,
  MatFormFieldModule,
  MatCardModule,
  MatIconModule, MatCheckboxModule, //icons lis: https://klarsys.github.io/angular-material-icons/
  MatInputModule, MatSelectModule,
  MatDialogModule,
  MatTableModule
  //MatProgressSpinnerModule
]

const angularPrimeng = [
  PasswordModule,
  ProgressSpinnerModule
]

/**
 * Este modulo importa los modulos comeuntes para todos los compoentes o modulos, principalmente
 * carga los esquemas visuales
 */

@NgModule({
    declarations: [DialogAnimationsExampleDialog],
    imports: [...angularMaterial,
      ...angularPrimeng],
    exports: [...angularMaterial
      ,...angularPrimeng],
      schemas:[CUSTOM_ELEMENTS_SCHEMA],
      providers:[DialogModalService,SpinnerService]
  })
export class ShareModule { }
