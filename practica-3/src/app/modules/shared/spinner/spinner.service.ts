import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
// se puede usar from 'rxjs' pero esto importaría todo los compoentes a ese nivel (sobrecarga)
import { Observer } from 'rxjs/internal/types';

@Injectable({providedIn: 'root'})
export class SpinnerService {
  // se crea un obsevable + observable
  observer!: Observer<boolean>;

  // se crea un observable y con el observer le pasamos el valor
  // al observable
  myObserbavle = new Observable<boolean>((observer: Observer<boolean>)=>{
    this.observer = observer;
    // asingmoas un valor al observable
    observer.next(false);
  });

  constructor() { }


  // funcion que maneja los cambios del observable
  public backApplication(event:boolean){
    // le cambiamos el valor al observable meidnate el obsever
    this.observer.next(event);
  }

  // funcion que devuele el observable
  // para que un elemento se suscriba y realice
  // los cambios
  public blockApplicationSuscribe(): Observable<boolean>{
    return this.myObserbavle;
  }

}
