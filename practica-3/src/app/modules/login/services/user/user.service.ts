import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, delay, of } from 'rxjs';


import { ResUser } from "../../interfaces/res-user-login-interface";
import { ReqUserLogin} from '../../interfaces/req-user-login-interface'
import { enviroment } from 'src/app/enviroment/enviroment'
import { userMock } from './user.mock';
/**
 * Servicio de la clase User para relaizar centrado a realizar
 * peticiones http.
 *
 * Debe declarase como proveedor en el modulo (requerie que se importe HttpClientModule si no http es nulo)
 */
@Injectable({
    providedIn: 'root'
})
export class UserService {

    url: string;


    constructor(private http:HttpClient) {
        this.url = enviroment.taskFlixUrl;
    }

    login(userReqLogin: ReqUserLogin): Observable<ResUser[]>{
        // this.http.post() es un observable
        if(!enviroment.userMock){// esto pemrite devolver el resutado con el serivico
            return this.http.post<ResUser[]>(this.url + "/users/loing",userReqLogin);
        }else{ // o un mock, con un retardo dado
            return of(userMock).pipe(
                delay(enviroment.delayMock)
            );
        }
    }



}
