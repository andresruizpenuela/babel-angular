import { ResUser } from "../../interfaces/res-user-login-interface";

// Modelo de datos de una respuesta del usuario al logearse
export const userMock: ResUser[]= [
    {
        user_id:1,
        name: "TEST",
        lastname:"MOCK",
        email: "tes@lab.com",
        password: "eeee",
        role: "USER",
        status: "ON",
        token: "TOKEN2"
    }
]