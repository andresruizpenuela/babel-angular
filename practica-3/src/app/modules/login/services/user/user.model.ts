import { Injectable } from "@angular/core";
import { ResUser } from '../../interfaces/res-user-login-interface';

/**
 * Esto es un servicio que contiente un objeto del modelo
 * el cual es compartido para todos los componentes que lo cargan
 * y se restablce cuando se recarga la pagina por completo (F5)
 * como cualquier otro servicio.
 *
 * Como no se va a utilizar más fuera del componente
 */
@Injectable({
    providedIn: 'root'
})
export class UserModel{
    private _user!:ResUser;

    setUser( user: ResUser){
        this._user = user;
    }

    getUser():ResUser | undefined {
        if(this._user) return this._user;
        // recuperamos si el objeto esta en la storage si no se devuelve vacio
        let user = JSON.parse(sessionStorage.getItem("users") || "" );
        return user;
    }
}
