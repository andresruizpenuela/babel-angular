import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { delay, map } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

import { UserService } from '../../services/user/user.service';
import { UserModel } from '../../services/user/user.model';
import { ResUser } from "../../interfaces/res-user-login-interface";
import { ReqUserLogin } from '../../interfaces/req-user-login-interface';
import { enviroment } from 'src/app/enviroment/enviroment';
import { SpinnerService } from 'src/app/modules/shared/spinner/spinner.service';



@Component({
  //standalone:true,
  selector: 'app-login',
  //imports:[ReactiveFormsModule,FormsModule,SharedModule],
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // grupo de formulario, se mapea en el fomr del template con [formGroup]="form"
  // requiere que se añade al login.module.ts: FormsModule, ReactiveFormsModule  de '@angular/forms';
  form!: FormGroup; // para el formulario reactivo, tiene la información del formulario completo

  private static _isLoading: boolean = false;
  /**
   * Inyeccion de depdencias en el constructor
   *
   * @param activateRoute instancia para manejar las rutas (parametros, querys ,..)
   * @param formBuilder instancia para formularios reactivos, requiere usar el ciclo de vida de Angular OnIngit y OnDestroy para manejar el templay de forma segura, así como importar en el modulo principal: FormsModule, ReactiveFormsModule de '@angular/forms';
   * @param userService instancia del servicio de usuario
   */
  constructor(private activateRoute:ActivatedRoute, private formBuilder: FormBuilder,private userService: UserService,
    private _snackBar: MatSnackBar,
    private userModel:UserModel,
    private router: Router,
    private spinnerService:SpinnerService){
    // acceso a los parametros
    // siconra (mediante get)
    console.log( this.activateRoute.snapshot.paramMap.get('idPersona') );

    // asincrona (subcribe)
    this.activateRoute.paramMap.subscribe(params =>{
      console.log( params );
    })

    // acceso al queryParam inserto en la query de forma asincrona
    this.activateRoute.queryParams.subscribe(queryParams =>{
      console.log( queryParams );
    })

    // acceso al url en la query de forma asincrona
    this.activateRoute.url.subscribe(url =>{
      console.log( url );
    })

    // acceso al data insert en la query de forma asincrona
    this.activateRoute.data.subscribe(data =>{
      console.log( data );
    })
  }

  ngOnInit(): void {
    // asegura que el compoente esta listo para ser usado

    // crear un grupo
    this.form = this.formBuilder.group({
      // campos del formulario (campos que se mapean al al formulairo del html)
      // nombre del campo (mapeado con formControlName del html) : [valores_iniciales,...validaciones]
      userName:["nombre",[Validators.required,Validators.minLength(4)]], // no se le da valor inicial, ni validaciones
      userEmail:[null,[Validators.required,Validators.email]],
      //password:[null,Validators.pattern("/^(?=.*[!@#$&*,.\-_/<>])(?=.*[0-9])(?=.*[^-Z]).(6,)$/")],
      password:[null],
    });


    console.log(this.form); //this.form.touch es false
  }

  onSubmitForm():void{
    // mostramos el spinner que es un elmento global
    this.setIsLoader(true);
    this.spinnerService.backApplication(this.isLoader());


    // usaruio unico en el back (mock)
    this.form.get('userName')?.setValue("Joaquin.ferreira");
    this.form.get('userEmail')?.setValue("joaquin.ferreira@babelgroup.com");
    this.form.get('password')?.setValue("12345Admin_");
    console.log("Estado del formulario: "+this.form?.status);
    console.log(this.form); // si se pulsa el botón y no se modifica el formuclairo, this.form1.touch es false, aunque se cambie por código (programaticamente)

    if(this.form.valid){ // si el formulario es valido, llamaral servicio
      // se monta el parametro
      let req:ReqUserLogin = {
        email:this.form.get('userEmail')?.getRawValue(),// "joaquin.ferreira@babelgroup.com",
        password:this.form.get('password')?.getRawValue() //"12345Admin_"
      }

      // como login deuvelve un observable, se suscribre y procesa la slaida
      this.userService.login(req).subscribe((res: ResUser[])=>{

        if(res.length>0){
          console.log(res);
          // recuperamos el usuario
          //this.userModel.setUsername(res[0]);
          this.userModel.setUser(res[0]);

          // se guarda el usuriao en el Sotrage de la aplicacion
          // para que enc caso de refrescar la pagina se mantega
          sessionStorage.setItem("users",JSON.stringify(res[0]));

          // redireccionar al dashboard que tiene un "guard"
          this.router.navigate(['/dashboard']);
        }else{
            this._snackBar.open('Error','close',{horizontalPosition: 'end',verticalPosition: "top"});
        }
        this.setIsLoader(false);
      })

    }else{

      this._snackBar.open('Error','close',{horizontalPosition: 'end',verticalPosition: "top"});
      delay(enviroment.delayMock*10);
      this.setIsLoader(false);
    }
  }

  /** Validacioens en Angular en la API
   * cuenta con una clase: https://angular.io/api/forms/Validators#validators
   *
   * class Validators {
      static min(min: number): ValidatorFn
      static max(max: number): ValidatorFn
      static required(control: AbstractControl<any, any>): ValidationErrors | null
      static requiredTrue(control: AbstractControl<any, any>): ValidationErrors | null
      static email(control: AbstractControl<any, any>): ValidationErrors | null
      static minLength(minLength: number): ValidatorFn
      static maxLength(maxLength: number): ValidatorFn
      static pattern(pattern: string | RegExp): ValidatorFn
      static nullValidator(control: AbstractControl<any, any>): ValidationErrors | null
      static compose(validators: ValidatorFn[]): ValidatorFn | null
      static composeAsync(validators: AsyncValidatorFn[]): AsyncValidatorFn | null
    }
   */

    public isLoader():boolean{
      return LoginComponent._isLoading;
    }
    public setIsLoader(isLoader:boolean):void{
      LoginComponent._isLoading = isLoader;
    }
}
