import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { LoginComponent } from './components/login/login.component';
import {LoginRoutingModule} from './routing/login-routing.module';
import { ShareModule } from '../shared/share.module';

import { UserService} from './services/user/user.service';


@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    ShareModule,
    HttpClientModule
  ],
  providers:[ UserService],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class LoginModule { }
