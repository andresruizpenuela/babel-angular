// modelo de la respusta
export interface ResUser{
    user_id:number,
    name: string,
    lastname:string,
    email: string,
    password: string,
    role: string,
    status: string,
    token: string
}