import { CUSTOM_ELEMENTS_SCHEMA, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Dropdown, PRIORITY_OPTIONS, TYPE_OPTIONS } from './constants'
import { TaskService } from 'src/app/api/task/task.service';
import { Task } from 'src/app/api/task/task.model';
import { DialogModalService } from 'src/app/modules/shared/dialogs-modal/dialog-modal.service';
import { SpinnerService } from 'src/app/modules/shared/spinner/spinner.service';


/**
 * Se usar NgModel en lugar de FormModule, para comunicar entre la  vista y el
 * componente, dado que no se requiere validación alguano.
 *
 * Importar en el modulo donde se declara el componente:
 * import {FormsModule} from '@angular/forms'
import { DialogModalService } from '../../../shared/dialogs-modal/dialog-modal.service';
 */

@Component({
    selector: 'app-dashboard-task',
    templateUrl: './task.component.html',
    styleUrls: ['./task.component.scss']
})

export class TaskComponent implements OnInit {
    @Input() task!: any; // se enviara a la vista por ngModel
    @Output() taskEmit:EventEmitter<Task> = new EventEmitter();

    typeOptions:Dropdown[] = TYPE_OPTIONS;
    priorityOptions:Dropdown[] = PRIORITY_OPTIONS;

    editModeOn:boolean = false;

    constructor(private taskSerive:TaskService,private dialogModalService:DialogModalService,
      private spinnerService:SpinnerService){ }

    ngOnInit() {
        console.log("tarea: "+JSON.stringify(this.task));
    }

    updateStateEditionMode():void{
        this.editModeOn=(!this.editModeOn);
    }

    saveTask():void{
      // muestra el spinner
      this.spinnerService.backApplication(true);
      // guardar tarea,enviarla al padre
      this.editModeOn=false;

      console.log(this.task);

      // subcribe, optiones aviable:
      // option A (clasic)
      /*
      this.taskSerive.saveTask(this.task).subscribe( result =>{
        console.log(this.task);
       });
       */
      // option B
       this.taskSerive.saveTask(this.task).subscribe({
          next: (result=>{
            /**
             * El resutlado tiene la siguiente pinta:
             * Donde affectedRows, nos indica el numero de filas modicadas
             * {
                  "fieldCount": 0,
                  "affectedRows": 1,
                  "insertId": 0,
                  "info": "Rows matched: 1  Changed: 0  Warnings: 0",
                  "serverStatus": 2,
                  "warningStatus": 0,
                  "changedRows": 0
              }
             */
            console.log(result);
            // le pasa el reustlado al componente padre
            this.taskEmit.emit(result);

            // oculta el spinner
            this.spinnerService.backApplication(false);
          }),
          error: (error=>{
            console.error(error);
            // oculta el spinner
            this.spinnerService.backApplication(false);
          })
        });
    }

    deleteTask():void{
      // muestra el spinner
      this.spinnerService.backApplication(true);

      //antesd e borrar mostrar un poup de angular material
      this.openDialog();
    }

    // abre el dialgo y muestra el resultado
    openDialog(): void {
        this.dialogModalService.showDialogModal("Delete Task","Do you like this task?")
          .subscribe(res=>{
            console.log(res);
            // res peude ser true o false, si es true se puede borrar la tarea
            // y se debe comunicar al padre
            // muestra el spinner
            this.spinnerService.backApplication(false);
          });
    }
}
