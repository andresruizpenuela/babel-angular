export interface Dropdown {
    value : string;
    label : string;
}

export const TYPE_OPTIONS: Dropdown[] =[
    {value: "task", label: 'Task'},
    {value: "fix", label: 'Fix'},
    {value: "info", label: 'Info'},
    {value: "bug", label: 'Bug'},
]

export const PRIORITY_OPTIONS: Dropdown[] =[
  {value: "low", label: 'Low'},
  {value: "mediun", label: 'Mediumo'},
  {value: "high", label: 'High'}
]
