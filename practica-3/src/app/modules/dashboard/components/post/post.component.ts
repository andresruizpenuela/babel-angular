import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';

import { postMock } from 'src/app/api/post/post-mock.model';
import { Post } from 'src/app/api/post/post.model';
import {MatTableModule} from '@angular/material/table';
import { PostService } from '../../../../api/post/post.service';

@Component({
  selector: 'app-post-component',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule
  ],
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostComponent implements OnInit {

  // prueba 1: creando los datos en el porpio componente
  // public displayedColumns: string[] = ['name', 'lastName'];
  // public dataSource = [{name:"Crhistian",lastName: "Rennovell"}];

  // pureba 2: tomando un mock de datos
  //public displayedColumns: string[] = ['userId', 'id', 'title', 'body'];
  //public dataSource:Post[] = postMock;
  public displayedColumns: string[] = ['userId', 'id', 'title', 'body'];
  public dataSource:Post[] = [];
  // prueba 3:  meidante sericio, requiere un refresco
  constructor(private postService: PostService, private changeDetectorRef: ChangeDetectorRef){

    this.postService.getData().subscribe(
          res => {
            this.dataSource = res;

            // hacemos que angular detectec el cambio
            this.changeDetectorRef.detectChanges();
          }
      );
  }
  ngOnInit(): void {
    console.log("init post component");


  }

 }
