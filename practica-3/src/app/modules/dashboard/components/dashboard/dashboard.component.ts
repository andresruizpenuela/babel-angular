import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/api/task/task.model';
import { TaskService } from 'src/app/api/task/task.service';
import { ResUser } from 'src/app/modules/login/interfaces/res-user-login-interface';
import { UserModel } from 'src/app/modules/login/services/user/user.model';
import { taksMock } from "../../../../api/task/taks.mock"
import { SpinnerService } from 'src/app/modules/shared/spinner/spinner.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  user?: ResUser;
  taskList?:Task[];

  constructor(private userModel:UserModel,
    private taskService:TaskService,
    private spinnerService:SpinnerService,
    private activedRoute: ActivatedRoute // recueprar los datos de la ruta
    ){}

  ngOnInit(): void {
    // oculta el spinner
    this.spinnerService.backApplication(false);

    // recuperar la infor del user si pasa el guardian
    //console.table(this.userModel.getUser());


    console.log("Dashboard Component");
    console.table(this.user);

    this.user=this.userModel.getUser(); // recupera el user, si no es del login recinete, de la sesion
    console.table(this.user);

    // recuperar la taks[] del resovle de las tareas (en esete caso es una promesa)
    console.log(this.activedRoute.snapshot.data); // data deuvelve un array donde los datos esta en la posición 0
    //console.log(this.activedRoute.snapshot.data['taskResovler']);  // en el curso, data devuelve un objeto que se llama tasResolver y no un array
    console.log(this.activedRoute.snapshot.data[0]); // se debe poner [INDICE DEL OJBETO O NOMBRE] en funcion de como lo devuelva
    this.taskList = this.activedRoute.snapshot.data[0];
    // pasa a un resolve
    /**

    if(this.user){//control of undifine
      this.taskService.getTask(this.user.user_id).subscribe((task:Task[])=>{
        console.table(this.userModel.getUser());
        console.table(task);
        if(task){// respuesta del back
          this.taskList = task
        }else{// mock the reponse
          this.taskList = taksMock;
        }

      });
    }
     */
  }

  saveTask(event:any):void{

    //this.taskList?.push(taskNew);
    console.log(event);

  }
}

