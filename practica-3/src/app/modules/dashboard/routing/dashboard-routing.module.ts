import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DashboardComponent} from '../components/dashboard/dashboard.component';
import {Hijo1Component} from '../components/hijo1/hijo1.component';
import {Hijo2Component} from '../components/hijo2/hijo2.component';
import {TaskComponent} from '../components/task/task.component';
import { AuthGuard } from 'src/app/guards/auth.guard';
import { taskResolver } from 'src/app/resolve/taskResolver.resolver';

export const routes: Routes = [{ // ...dashboard, ...dashboard/hijo1, ...dashboard/hijo2
    canActivate:[AuthGuard],// aqui se ejecutario en /dashbord
    resolve: [taskResolver], // esto comprueba que se resuelva algo (como cargar datos) antes de mostrar la vista, si no lo resuelve no entra, como la promesa AuthGuard.canActivate()
    path: 'dashboard',
    component: DashboardComponent,
    children:[{ // route to componente children, esto implica que tambien se usa RouterMoudle.forChild(routes)
      path: 'hijo1',      component: Hijo1Component
    },{ // route to componente children
      path: 'hijo2',      component: Hijo2Component
    },{ // route to componente children
      path: 'task',      component: TaskComponent
    }
  ]
}];

@NgModule({
    declarations:[Hijo1Component], // se debe declar los componetes aqui o en el modulo que importa este modulo
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
export class DashboardRoutingModule { }
