import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { CommonModule } from '@angular/common';


import { DashboardRoutingModule } from './routing/dashboard-routing.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { Hijo1Component } from './components/hijo1/hijo1.component';
import { Hijo2Component } from './components/hijo2/hijo2.component';
import { TaskComponent } from './components/task/task.component';
import { ShareModule } from '../shared/share.module';
import { PostComponent } from './components/post/post.component';

@NgModule({
  declarations: [DashboardComponent,Hijo2Component,TaskComponent],
  imports: [
    PostComponent, // como es un standalone component, se debe importar
    CommonModule,
    FormsModule,
    DashboardRoutingModule,
    ShareModule // aunque este declarado en un nivel superiro (ya sea en el modulo principal, es requeido decalarlo en este modulo)
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class DashboardModule { }
