/**
 * Clase para seguridad en la autenticación (validaicones,... ) acuta de proxy
 */
import { Injectable, ResolvedReflectiveFactory } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from '../modules/login/services/user/user.model';

/**
 * AuthGurad usado para Desbhoard, en el routing:
 * 
 * <code>
 * { // loading ngmoudles 
 *  path: 'dashboard',
 *  canActivate: [AuthGuard], // si es false no deja entrar '/dashobard'
 *  loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
 * }
 * </code>
 */

@Injectable({
    providedIn:'root'
})
export class AuthGuard {
    /**
     * 
     * @param router instanica para las ridecctiones
     * @param user model of user
     */
    constructor(private router:Router, private userModel:UserModel){}

    // caso de sincrono (sin pormesa), asincorno (con promeas)
    canActivate(){
        // sinconro
        /*
        let isauth: boolean = false; // si es true para avanzar y acceder al la ruta, si no no deja entrar

        // si es false se puede redireccionar, con 'skipLo...' se oculta
        if(!isauth){ this.router.navigate(['/login'],{skipLocationChange:true});}

        return isauth;
        */
        // asinconro
        return new Promise((resolve,reject)=>{
            /** 
            let isauth: boolean = false; // si es true para avanzar y acceder al la ruta, si no no deja entrar
            
            // condicón de login ok
            isauth = this.userModel.getUser()?true:false;

            // simulamos un tiempo de respusta
            setTimeout(()=>{
                // si es false se puede redireccionar, con 'skipLo...' se oculta
                if(!isauth){ 
                    this.router.navigate(['/login'],{skipLocationChange:true});
                    reject(isauth);// is false
                }
                resolve(isauth);// is true
            },2);// 2 ms of wait
            */
            console.log("AuthGarud");
            console.table(this.userModel.getUser());
            
            if(this.userModel.getUser()){
                resolve(true)
            }else{
                this.router.navigate(['/login'],{skipLocationChange:true});
                reject(false);// is false
            };
        });
    }
}