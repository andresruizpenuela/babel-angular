import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { LoginModule} from './modules/login/login.module';
import { DashboardModule} from './modules/dashboard/dashboard.module';
import { ProfileModule} from './modules/profile/profile.module';

import { TaskService } from './api/task/task.service';
import { ShareModule } from './modules/shared/share.module';
import { PostComponent } from './modules/dashboard/components/post/post.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ServerInterceptor } from './api/interceptors/server.interceptor';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    LoginModule,      // lazy loading ngmodule
    DashboardModule,  // lazy loading ngmodule
    ProfileModule,    // lazy loading ngmodule
    AppRoutingModule, BrowserAnimationsModule,
    ShareModule
  ],
  providers: [TaskService,
    // ejemplo de interceptor: Interceptar todas las peticiones HTTP y la manejara la clase defina en useClass
    {provide: HTTP_INTERCEPTORS, useClass: ServerInterceptor,multi:true}],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
