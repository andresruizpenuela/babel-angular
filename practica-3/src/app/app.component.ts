import { Component } from '@angular/core';
import { SpinnerService } from './modules/shared/spinner/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'practica-3';

  isLoading:boolean = false;
  constructor(private spinnerService:SpinnerService){

    // se suscribe al evento y cambia el estado según el valor recibe del observer.
    this.spinnerService.blockApplicationSuscribe().subscribe(res=>{
        this.isLoading = res;
    })
  }


}
