import { UserService } from '../modules/login/services/user/user.service';



import { inject } from '@angular/core';
import type { ResolveFn } from '@angular/router';
import { TaskService } from '../api/task/task.service';
import { UserModel } from '../modules/login/services/user/user.model';
import { Task } from '../api/task/task.model';
import { taksMock } from '../api/task/taks.mock';

// Tendia ha hacerlo con funciones, aunque puede ser una clase con resolver


export const taskResolver: ResolveFn<any> = (route, state) => {
  // el objetio será recuperar el usuario y devolverlo a la vista
  //como es una funcion no se inyecta los servicios, si no se crea constantes de tipo inject(service)
  const userSerive = inject(UserModel);
  const taskService = inject(TaskService);

  // como es el observable es asinconro y la funcion es sincrona,
  // para reperar el relsuddo del observable, se debe intorducir
  // en el resolve de la promesa, es decir,
  // cuando termina la promesa,entra en el la tarea
  return new Promise<Task[]>((resolve,rejects)=>{
      // se recuepra el user_id
      let user_id = userSerive.getUser()!.user_id;
      taskService.getTask(user_id).subscribe(result =>{
          // se deuvelve el resultado asincrono
          // en una funciono no asincrona
          resolve(result);
      });
  });
  //return taksMock;
  // devuelve la lista (no hace falta promesa, porque se recupera de un objeto sincrono)
  // vamos que los datos que se quieren recueprar no debemos esperar a que se cargen de
  // otro servicio (observable)

};
