import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {AuthGuard} from './guards/auth.guard';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  { // loading ngmoudles 
    path: 'login/:idPersona', // parametro en la url
    data: {titulo: "Login con parametros"}, //pasa un objeto dentro de la query
    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule)
  },
  { // loading ngmoudles 
    path: 'login/', // parametro en la url
    data: {titulo: "Login sin parametros"}, //pasa un objeto dentro de la query
    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule)
  },
  { // loading ngmoudles 
    path: 'dashboard',
    //canActivate: [AuthGuard], // si es false no deja entrar '/dashobard', añadido esto en el dahboard-routing.module.ts
    loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  { // loading ngmoudles 
    path: 'profile',
    loadChildren: () => import('./modules/profile/profile.module').then(m => m.ProfileModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
