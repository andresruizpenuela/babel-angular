import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { enviroment } from 'src/app/enviroment/enviroment';


import { Task}  from './task.model';
@Injectable({
    providedIn: 'root'
})
export class TaskService {

    url: string;
    constructor(private httpClient:HttpClient) {
        this.url = enviroment.taskFlixUrl;
    }

    getTask(userId: number):Observable<Task[]>{
        return this.httpClient.get<Task[]>(this.url + "/tasks/user/" + userId);
    }

    // htpp://.../tasks/id
    saveTask(taskReq:Task):Observable<any>{
      return this.httpClient.put<any>(this.url+"/tasks/"+taskReq.id,taskReq);
    }

}
