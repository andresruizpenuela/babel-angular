export interface Task{
    id: number;
    type: string;
    priority: string;
    description: string;
}
