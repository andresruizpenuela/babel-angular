
import { Task } from "./task.model";

// Modelo de datos de una respuesta del usuario al logearse
export const taksMock: Task[]= [
    {
        id: 6,
        type: "bug",
        priority: "high",
        description: "task high"
    },
    {
        id: 8,
        type: "bug",
        priority: "mediun",
        description: "task medium"
    }
]
