import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable  } from 'rxjs';


import { Post } from './post.model';
import { enviroment } from 'src/app/enviroment/enviroment';

@Injectable({providedIn: 'root'})
export class PostService {

  constructor(private httpClient:HttpClient) { }


  public getData():Observable<Post[]>{
      return this.httpClient.get<Post[]>(enviroment.tipeCodeUrl);
  }

}
