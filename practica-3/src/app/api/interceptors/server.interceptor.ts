
import type { HttpEvent, HttpHandler, HttpInterceptor, HttpInterceptorFn, HttpRequest } from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';
import { enviroment } from 'src/app/enviroment/enviroment';

export class ServerInterceptor implements HttpInterceptor{

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // cada peiticon http entra aquí
    console.log(req);

    // añaidmos la cabeceras a la petiicones http
    let httpHeaders;
    if(req.url.includes(enviroment.taskFlixUrl)){ // para las peticiones a taskFlixUrl

      httpHeaders = req.headers.set("Subscription","788978855jj888s5e");

    }else{
      httpHeaders = req.headers.set("Ocp-Apim-Subscription-key","788978855jj888s5e");
    }

    req = req.clone({
      headers: httpHeaders
    })


    // realizamos la petición y devolermos la respuesta,
    // si se prodcue un erorr (404,..), lo gestianmos por medio del "pipe"
    return next.handle(req).pipe(catchError(err =>{
      // gestion de error
      const error = err.message ||err.statusText;
      debugger // punto para parar
      return throwError(()=>error);
    }));
  }

}

