# Instalar WSL
Autenticamos con el usuario de adminsitrador del equipo y abrimos la powershell.

Ejecutamos
```shell
wsl --install
```

Reiniciamos el equipo para que termine la instalación y nos autenticamos con nuestro usuario corporativo.
Forzamos a que por defecto usemos WSL2 (por defecto deja la versión 1 que da problemas con docker) con
```shell
wsl --set-default-version 2
```

Instalamos ubuntu con
```shell
wsl --install -d Ubuntu
```
Generamos usuario y contras

*nota*: user creado >> user: user, pwd: root

Instalar nvm en Linux:
```shell
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
```

Instalar wsl en Visual stduio [wsl-vs](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl)

# Version de Node
nvm list

    18.18.1 (Currently using 64-bit executable)
  * 12.22.12
nvm use 18.18.1

# Version de angualar cli
nota:   remove any version: npm uninstall @angular/cli@XXXXX
        last version:       npm install -g @angular/cli

Install any version: npm i -g @angular/cli@15.2.7


# Crear proyecto

cd <path-project>
ng new <name-project>


```shell
...\NodeAngular12>cd Babel24
...\Babel24> ng new practica-1 

# Para abrir visual studio desde la consola (ctrl+shift+p y escribir "code", dar la opción instlar code en la path)
...\Babel24> cd practica-1
...\Babel24\practica-1> code -1 
```
# Lanzar proyecto

Con angular cli:    ng serve -o
Con node:           npm start   (lanza la conf del package json)

# app.module.ts

* Modulo principal
* Clase con el decorador de modulo, donde se indica los componentes, servicios, ..., usados por la aplicación
* Es el especificado por el ficho princial que se lanza al ejecutar la aplicaicón "main.ts"

# Crear componente

cd <path-project/name-project>
ng g c [<path/name-componet> | <name-componet>]

```shell
...\Babel24\practica-1> ng g c header
CREATE src/app/header/header.component.html (21 bytes)
CREATE src/app/header/header.component.spec.ts (599 bytes)
CREATE src/app/header/header.component.ts (203 bytes)
CREATE src/app/header/header.component.scss (0 bytes)
UPDATE src/app/app.module.ts (475 bytes)
```

Estructura creada:
practica-1
  | src
      | app
         | header

Nota: El comopone es registrado automaticamente con el ng

```javascript
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

# Compoente

* Clase con el decorador @Componente, el cual, tiene tres parametors princiaples:
** 'selector': etiequta con la que se nombrara el componene en la vista y resto de componentes
** 'template', 'tempaleUrl': Codigo o ruta de la vista del compoennte
** 'style','styleUrls': Codigo o ruta de la hoja de estilos usada para el componente

```javascript
import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

}
```

Uso del compenente en otra vista: Basta con poner el nombre del selector en la vsita
```html
<!-- index.html -->
<app-header></app-header>
<app-footer></app-footer>
<router-outlet></router-outlet>
```

# Instalar Angular Material

Instlación por npm: https://material.angular.io/, para angular 15 instalar la misma verisón

[Material](https://material.angular.io/) > Aqui podemos ver las veriosnes disponibles
[NPM](https://www.npmjs.com/package/@angular/material/v/15.0.0) > Aquie el cmando

Nota: Si es anguar 15 instalar la verisón 15 mas alta de material

Para Angular 15, instalamos la ulitma vesion cuyo nmero mayor sea igual:
```shell
npm i @angular/material@15.2.9 
ng add  @angular/material@15.2.9

# nos dirá el siguiente formulario:
Skipping installation: Package already installed
? Choose a prebuilt theme name, or "custom" for a custom theme: 
Indigo/Pink        [ Preview: 
https://material.angular.io?theme=indigo-pink ]
? Set up global Angular Material typography styles? Yes
? Include the Angular animations module? Include and enable 
animations
UPDATE package.json (1129 bytes)
⠴ Installing packages (npm)...

# Nota forzar la instalación
npm i @angular/material@15.2.9  --force or --legacy-peer-deps
```
Para usar material se importa en el modulo del componente que utiliará esta api

@Component({
  importas: [
      MatButtonModule,
      ...
  ]
})
export class ....

Alternativa a AngularMaerial: Boostrap, [Primeng](https://www.primefaces.org/primeng-v15-lts/)

## Instalar Boostrap
En "index.html",añadir:

```html
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
```

Nota: Se pude instlar también con "ng"

## Instar Primeng

Via ng/npm:

```shell
npm i npm i primeng@15.4.1 --force or --legacy-peer-deps
ng add npm i primeng@15.4.1
```

Nota: La version "lts" muestra un mensaje de no usarlo


# Url del servidor node:
Montar una base de datos MySQL: https://railway.app/
Servidor Node: https://taskflix-api.onrender.com

# Servicios en Angular