# Instalar nvm (windows)
con ejecutable "nvm-setup.exe" [link](https://github.com/coreybutler/nvm-windows)

# Instalacion node con nvm (windows)

1. Revisa qué versiones de Node.js están disponibles.
```
nvm ls-remote
#o 
nvm list available
```

2. Instala cualquier versión de Node.js que desees.
```
nvm install v12.22.7
```

3. Establece tu versión actual de node a tu nueva versión:
```
nvm use v12.22.7
Now using node v12.22.7 (npm v)
```
4. Comprueba qué versión de Node.js se está ejecutando ingresando lo siguiente:
```
node -v
v12.18.3
```
recomendación: usar la última versión estable "LTS"


# Instalar TypeScript & Angular/CLI
Por medio del gestor "npm", si no se indica versión, se instala la ulitma

```
npm i -g typescript
npm i -g @angular/cli
```

nota: para actualizar la ultima versión, ejm `npm update`, `npm i -g typescript@latest`, `npm i -g @angular/cli@latest`

helper: `tsc -v`, `ng version`

## Versiones compatibles
[Link](https://angular.io/guide/versions)

Así por ejemplo :
ANGULAR	            NODE.JS	                                TYPESCRIPT	    RXJS
14.2.x || 14.3.x	^14.15.0 || ^16.10.0	                >=4.6.2 <4.9.0	^6.5.3 || ^7.4.0
14.0.x || 14.1.x	^14.15.0 || ^16.10.0	                >=4.6.2 <4.8.0	^6.5.3 || ^7.4.0
13.3.x	            ^12.20.0 || ^14.15.0 || ^16.10.0	    >=4.4.3 <4.7.0	^6.5.3 || ^7.4.0

```
npm i -g typescript@4.6.4
npm i -g @angular/cli@13.3.11
```

Para desistalar: 
```
npm uninstall -g typescript@4.6.4
npm uninstall -g @angular/cli@13.3.11
```

> EN el curso de babel se usa angular 15.2.0, por lo que se requiere node 18.10.0, typescript 4.9
> 
> ANGULAR	            NODE.JS	                                TYPESCRIPT	    RXJS
> 15.1.x || 15.2.x 	    ^14.20.0 || ^16.13.0 || ^18.10.0 		>=4.8.2 <5.0.0 	^6.5.3 || ^7.4.0
 
nvm install 18.10.0
npm i -g typescript@4.9
npm i -g @angular/cli@15.2.0

# Crear proyecto Angular

1º Ir al directorio donde se alojara el proyecto
2º Abrir un terminal
3º Lanzar `ng new <name-proyect>`
4º Completar los pasos (por defecto no usar google-analisis, usar routing, usar scss o css)

```
...\Angular\babel-angular\codes
...\Angular\babel-angular\codes> ng new angular-prj1
```

Importante: En Angular 17, los componentes ya no es requerido seguir el patrón de versiones anteriores, en la 
que los componentes se agregan a módulos (module-less), si no que por defecto "standalone=true", para crear proyects
en Angular 17, con el patrón de módulos usar la opicón `--standalone false" al crear el proyecto con ng.

```
ng new <nombre-proyecto> --standalone false
```
**Atajo vs**: Abrir un proyecto desde el termianl con VS usar el comando "code ." (esto requiere añadir visual studio
al path (ctrl + shift + p, escribir code e indicar "install code path...")

# Arrancar proyect

Vía ng:
```
ng serve -o
```

Via npm (esto arranca el comando "start" del package.json:
```
npm start
```
Nota: Modificar el "pacakage.json>scripts>start", y poner "ng serve -o", para que abra el navegador
cuando termine de arrancar