import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-directivas',
    templateUrl: 'directivas.component.html'
})

export class DirectivasComponent implements OnInit {
    condition:number = 1;
    fruta:string[] = ['pera','manzana','fresa','naranja','melon','sandia'];

    conditionExpressionSwith:number = 1;

    textNgModel:string = "";  // FormModule: NgModel
    constructor() { }

    ngOnInit() { }
}