import { HijoComponent } from './../hijo/hijo.component';
import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChildren } from "@angular/core";
import { UserService } from '../../service/user.service';

@Component({
  selector:'app-padre',
  templateUrl:'./padre.component.html',
  styleUrls:['./padre.component.scss']

})
export class PadreComponent implements AfterViewInit, OnInit{
    tituloHijo = 'propiedad hacia el hijo'

    // para forzar que detecte los cambios
    constructor(private changeDetectorRef:ChangeDetectorRef,private userSerivce:UserService){}

    // recibir todos los parametros de un compoennte, requiere un import del compoennte
    // debe estar aseguro que el componente que se importa se carge antes, si no se puede
    // usar y no se asegura, ademas, para detectar los cambios que se produzcan en el hijo
    // se debe usar el servcio 'changeDetectorRef', que fuerza a angualr que detecto
    // los cambios de las popaidres del hijo, pero con el ViewCHildren no se puede
    // detectar los cambios del hijo (no es reactivo), para eso "emitir" o "subcritos",
    // es decir, para propiedades que son dinamicas y queremos que se refleje en otro
    // componente
    @ViewChildren('HijoComponent') child ?:HijoComponent;
    nameHijo?:string = "";


    // recibir una propiedad del hijo meidante un emiter
    onNewEmit(event: string){
        console.log(event);
    }

    // asegura que el componente importado con View este cargado
    ngAfterViewInit(): void {
        this.nameHijo=this.child?.name;
        console.log(this.child?.name);
    }

    ngOnInit(){
      this.userSerivce.setUserName("Alejandro");
    }
}
