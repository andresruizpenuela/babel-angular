import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { UserService } from "src/app/service/user.service";

@Component({
  selector:'app-hijo',
  templateUrl:'./hijo.component.html',
  styleUrls:['./hijo.component.scss']

})
export class HijoComponent implements OnInit{
  // comunicar de padrea hijo
  @Input() title:string | undefined; // title?:string, se indica que sea opcioanl porque no se inializa o se le da un valor por defecto, para ello: @Input('title') title:string = ""
  // tips: que tenga un input no es obligatorio que el padre se lo pase, si no lo recibe del padre, es undefined
  //       Solo Angular >=16 => @Input({ required: true }) myRequiredInput!: unknown;


  // comunicar de hijo a padre
  @Output() newEmited = new EventEmitter<string>(); // requiere un metodo en el componente
  name?:string = "andres"; // esta propiead no se emite, pero se puede ver con ViewChield (no reavtivo, ciclo de vida critio), Servicios (reactivo), ...


  // servicio compartido, es una intancia estatico por lo que es común para todos los
  // compoentes que lo use, y se reinica cada vez que se refresca la página,
  // se utiliza como un sitema de almacenaminetolocal
  constructor(private userSerivce:UserService){}

  ngOnInit(): void {
      console.log(this.title);

      // cambiamos la propiedad name pasado X seg, no se refleja los cambios en
      // los componetes que acceden por ViewChage
      setTimeout(()=>{
        this.name = "andres update";
      },25);


      // accedmos al nombre l usruio parado X seg, se refela los cambios de otros componetes
      // si se ha modificado antes de ser moficiado, ya que esta guaradao en el servicio
      // que es como una "clase estatica" para todos los componetes que usen dicho servicio
      setTimeout(()=>{
        this.name = this.userSerivce.getUserName();
        console.log(this.name);
      },25);
  }

  // por defecto es pulico
  emitValue(){
    this.newEmited.emit("emitido por el hijo");

  }

}
