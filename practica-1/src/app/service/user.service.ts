import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class UserService {

  private _userName?:string;
  constructor() { }

  getUserName():string|undefined{
    return this._userName;
  }

  setUserName(userName:string|undefined){
    this._userName = userName;
  }

}
