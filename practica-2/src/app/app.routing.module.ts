import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//import {LoginComponent} from './components/login/login.component';
//import {DashboardComponent} from './components/dashboard/dashboard.component';
import { DashboardModule } from './components/dashboard/dashboard.module';
import {ProfileComponent} from './components/profile/profile.component'

// Rutas
export const routes: Routes = [
    // cuando entre vacio se diriga a /login
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    { // carga perezosa, el compoennte debe ser standalone (usado para v17)
        path:'login',
        loadComponent:() => import('../app/components/login/login.component').then(m => m.LoginComponent) 
    },
    { // carga perezosa, el compoentne pertenece a un modulo
        path: 'dashboard',
        loadChildren:() => import('../app/components/dashboard/dashboard.module').then(m=>m.DashboardModule) 
    },
    {
        path: 'profile',
        component:ProfileComponent // se debe importar (forma declarada)
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{

}
