import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';


import { RouterModule } from '@angular/router';



import {AppComponent} from './app.component';
import { AppRoutingModule } from './app.routing.module';

import { ProfileComponent } from './components/profile/profile.component';
//import { LoginComponent } from './components/login/login.component';
//import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardModule } from './components/dashboard/dashboard.module';



@NgModule({
  declarations: [
    AppComponent,
    //LoginComponent, // Compnente standalon, se gestiona solo
    //DashboardComponent,
    ProfileComponent,
  ],
  imports: [
    RouterModule,
    BrowserModule,
    FormsModule,
    DashboardModule, 
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
