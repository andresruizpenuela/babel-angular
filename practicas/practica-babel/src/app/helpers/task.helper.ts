import { taskList } from '../models/mocks/task-mock.model';
import { Task } from '../models/task/task.model';
export class TaskHelper{


  /**
   * Method that clone a list of Task, before, addiction a new task
   * as result, return the new list
   *
   * @param task to addtion
   * @param taskList to modify
   * @returns list of task with new task
   */
  static addTask = (task:Task,taskList:Task[]):Task[] =>{
    const cloneTaskList:Task[]  = Object.assign([], taskList);
    debugger
    // add the id
    if(task.id!=0){
      cloneTaskList.forEach((obj, i) => { if (obj.id == task.id) cloneTaskList[i] = task; });
    }else{
      let indexLastElement =0;
      if(cloneTaskList.length!=0){
        indexLastElement = cloneTaskList.length-1
        task.id  = cloneTaskList[indexLastElement].id + 1;
      }else{
        task.id= 1;
      }

      cloneTaskList.push(task);
    }


    return cloneTaskList;
  }

  /**
   * This method remove the a task given its id, if present in the list,
   * and return the list, if the id isn't in the list, then launch to error
   *
   * @param id identificate of task to remove
   * @param taskList list to modificate
   * @returns list with the task delete
   */
  static removeTask = (id:number,taskList:Task[]):Task[] =>{
    let cloneTaskList:Task[]  = Object.assign([], taskList);
    const task:Task | undefined = cloneTaskList.filter(task=>task.id == id)?.pop();
    if(task) {
      cloneTaskList = taskList.filter(obj =>{ return obj !== task });
     }else{
      throw new Error('The id: '+id+', not found in the list.');
    }
    return cloneTaskList;
  }

  /**
   * This method update the state the task to complete given
   * its id
   * @param id task to update its state
   * @param taskList list to task
   * @returns list to task
   */
  static completeTask = (id:number,taskList:Task[]):Task[] =>{

    const cloneTaskList:Task[]  = Object.assign([], taskList);

    cloneTaskList.forEach(task => {
      if(task.id == id){
        task.completed=true;
      }
    });

    return cloneTaskList;
  }


  /**
   * This procediment show for console the list
   * of task given
   *
   * @param taskList list of task
   */
  static printTask(taskList:Task[]):void{

    taskList.forEach(task=>console.log(task))
  }

}
