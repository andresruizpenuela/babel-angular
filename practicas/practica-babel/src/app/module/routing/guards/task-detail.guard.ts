import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Router } from "@angular/router";


@Injectable({providedIn: 'root'})
export class TaskDetailGuard {


  constructor(private router: Router){}

  canActivate(route: ActivatedRouteSnapshot) {

    if (!route.params['id'] && !Number(route.params['id'])) {
      this.router.navigate(['/task']);
    }
    return route.params['id']?true:false;
  }
}
