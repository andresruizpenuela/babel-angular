import { NgModule } from '@angular/core';
import { RouterLink, RouterLinkActive, RouterModule, RouterOutlet } from '@angular/router';
import { routes } from './model/routing.model';

const routerModules = [RouterOutlet, RouterLink, RouterLinkActive];
@NgModule({
  imports: [...routerModules, RouterModule.forRoot(routes)],
  exports: [...routerModules, RouterModule],
  declarations: [],
  providers: [],
})
export class AppRoutingModule { }
