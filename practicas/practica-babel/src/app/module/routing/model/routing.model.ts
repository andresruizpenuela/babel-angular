import { Routes } from '@angular/router';
import { TaskComponent } from '../../../components/task/task.component';
import { TaskDetailComponent } from '../../../components/task-detail/task-detail.component';
import { TaskDetailGuard } from '../guards/task-detail.guard';

export const routes: Routes = [
  { path: '',   redirectTo: 'task', pathMatch: 'full' }, // redirect to `/`
  { path: 'task', component: TaskComponent },
  { path: 'detail/:id', canActivate: [TaskDetailGuard], component: TaskDetailComponent },
];
