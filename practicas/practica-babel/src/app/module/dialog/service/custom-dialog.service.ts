
import { Injectable, OnDestroy } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';

@Injectable({providedIn: 'root'})
export class CustomDialogService {

  ref!: DynamicDialogRef;

  constructor(
    public dialogService: DialogService,
    public messageService: MessageService
  ) { }


  show(header:string,data:any) {
    console.log("dialog service");

    this.ref = this.dialogService.open(CustomDialogService, {
        header: header,
        data: data,
        width: '70%',
        //closable: true,
        contentStyle: { 'max-height': '500px',
                         overflow: 'auto',
                       },
        baseZIndex: 10000,
    });
  }
/*
  ngOnDestroy() {
    if (this.ref) {
        this.ref.close();
    }
}*/

}
