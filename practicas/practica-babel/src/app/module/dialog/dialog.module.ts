import { NgModule } from '@angular/core';


import { DialogService } from 'primeng/dynamicdialog';
import { ConfirmationService, MessageService } from 'primeng/api';

import { CommonModule } from '@angular/common';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ToastModule } from 'primeng/toast';
import { CustommDialogComponent } from './component/custom-dialog/custom-dialog.component';


@NgModule({
  imports: [CommonModule,ConfirmDialogModule,ToastModule],
  exports: [ConfirmDialogModule,ToastModule],
  declarations: [CustommDialogComponent],
  providers: [ConfirmationService, MessageService,DialogService, MessageService,DialogService],
})
export class DialogModule { }
