import { Component, Input, OnInit } from '@angular/core';
import { ConfirmationService, MessageService, PrimeNGConfig } from 'primeng/api';
import { TaskService } from '../../../../services/task.service';
import { Router } from '@angular/router';

@Component({
  selector: 'confirm-delete-dialog',
  templateUrl: './confirm-delete-dialog.component.html'
})

export class ConfirmDeleteDialogComponent implements OnInit {

  @Input()
  idTask!:number;


  constructor(private confirmationService: ConfirmationService,private messageService: MessageService,
    private taskService:TaskService, private router: Router) { }

  ngOnInit() {

  }

  delete(event: Event) {
    this.confirmationService.confirm({
      target: event.target as EventTarget,
      message: 'Desea borrar la task con id: '+this.idTask+'?',
      header: 'Borrado',
      icon: 'pi pi-exclamation-triangle',
      acceptIcon:"none",
      rejectIcon:"none",
      rejectButtonStyleClass:"p-button-text",
      accept: () => {
          this.messageService.add({ severity: 'info', summary: 'Confirmed', detail: 'You have accepted', life: 3000 });
          this.taskService.removeTask(this.idTask);
          setTimeout(() =>
            {
              this.router.navigate(['/task']);
            },
            3000/2);
      },
      reject: () => {
          this.messageService.add({ severity: 'error', summary: 'Rejected', detail: 'You have rejected', life: 3000 });
      }
  });

  }

  close() {
      this.confirmationService.close();

  }

}
