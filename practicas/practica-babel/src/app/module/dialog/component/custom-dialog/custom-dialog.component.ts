
import {  DynamicDialogComponent, DynamicDialogRef,DynamicDialogConfig } from 'primeng/dynamicdialog';
import { Component, OnDestroy } from '@angular/core';
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: 'custom-dialog.component.html',
  styleUrls: ['./custom-dialog.component.css']
})
export class CustommDialogComponent {

  id:number = 0;
  instance: DynamicDialogComponent | undefined;

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
    console.log(this.config.data?.id)
    this.id = Number(this.config.data?.id)? Number(this.config.data?.id):0;
  }


  ngOnInit() {

  }

  close() {
    this.ref.close();
  }
}
