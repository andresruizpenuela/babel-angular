import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from '../module/dialog/dialog.module';
import { FormTaskGroupComponent } from '../components/form-task-group/form-task-group.component';

const primeng = [TableModule,CardModule,ButtonModule]

@NgModule({
  imports: [ DialogModule, FormsModule, ...primeng ],
  exports: [ DialogModule, FormsModule, ...primeng ],
  declarations: [],
  providers: [],
})
export class SharedModule { }
