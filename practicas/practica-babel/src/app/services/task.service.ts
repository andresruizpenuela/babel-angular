import { Injectable } from '@angular/core';
import { taskList } from '../models/mocks/task-mock.model';
import { Task } from '../models/task/task.model';
import { TaskHelper } from '../helpers/task.helper';

@Injectable({providedIn: 'root'})
export class TaskService {

  private nameFieldsTask:string[] = ['name','completed'];
  private shareTaskList:Task[] = taskList;

  constructor() { }

  getNameFields = ():string[] => this.nameFieldsTask;

  getAllTask = ():Task[] => this.shareTaskList;

  getTask = (id:number):Task | undefined => this.shareTaskList.find(task=>task.id==id);

  addTask = (task:Task):void =>{ this.shareTaskList = TaskHelper.addTask(task,this.shareTaskList); }

  removeTask = (id:number):void =>{ this.shareTaskList = TaskHelper.removeTask(id,this.shareTaskList); }


}
