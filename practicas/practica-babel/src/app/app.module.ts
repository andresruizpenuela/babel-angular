import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { TaskService } from './services/task.service';
import { TaskComponent } from './components/task/task.component';
import { SharedModule } from './shared/shared.module';
import { FormTaskComponent } from './components/form-task/form-task.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskDetailComponent } from './components/task-detail/task-detail.component';

import { AppRoutingModule } from './module/routing/app-routing.module';
import { ConfirmDeleteDialogComponent } from './module/dialog/component/confirm-delete-dialog/confirm-delete-dialog.component';
import { FormTaskGroupComponent } from './components/form-task-group/form-task-group.component';


@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    TaskListComponent,
    TaskDetailComponent,
    FormTaskComponent,
    ConfirmDeleteDialogComponent,
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule,  SharedModule, AppRoutingModule,FormTaskGroupComponent
  ],
  providers: [TaskService],
  bootstrap: [AppComponent]
})
export class AppModule {}
