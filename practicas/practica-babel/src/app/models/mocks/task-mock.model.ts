import { Task } from '../task/task.model';
export const taskList:Task[] = [
  {  id: 1, name: 'Task 1', completed: false, detail:' Detial task 1'},
  {  id: 2, name: 'Task 2', completed: true, detail:' Detial task 2' },
  {  id: 3, name: 'Task 3', completed: true, detail:' Detial task 3' },
  {  id: 4, name: 'Task 4', completed: false, detail:' Detial task 4' },
  {  id: 5, name: 'Task 5', completed: false },
  {  id: 6, name: 'Task 6', completed: true }
]
