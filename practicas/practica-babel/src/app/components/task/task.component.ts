import { Component, Input, OnInit } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { Task } from 'src/app/models/task/task.model';

@Component({
  selector: 'app-task',
  templateUrl: 'task.component.html'
})

export class TaskComponent implements OnInit {


  protected taskList!:Task[];

  protected taskNameFileds!:string[];

  @Input()
  taskInput:Task = {id:0,name:'',completed:false};



  constructor(private taskService:TaskService){  }

  ngOnInit() {
    this.taskNameFileds= this.taskService.getNameFields();
    this.recoverTaskList();
  }

  recoverNameFields = ():string[] => this.taskService.getNameFields();
  recoverTaskList = ():void=> { this.taskList = this.taskService.getAllTask();}
  editTask = (task:Task):Task => this.taskInput=task;

  addTask = (task:Task):void =>{
    this.taskService.addTask(task);
    this.recoverTaskList();
    this.taskInput = {id:0,name:'',completed:false};
  }

  removeTask = (id:number):void =>{
    console.log('borrar task id: '+id)
    this.taskService.removeTask(id);
    this.recoverTaskList();
  }


}
