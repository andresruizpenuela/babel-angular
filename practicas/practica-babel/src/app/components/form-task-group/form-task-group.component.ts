import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Task } from 'src/app/models/task/task.model';
import { TaskService } from 'src/app/services/task.service';
import { taskList } from '../../models/mocks/task-mock.model';

@Component({
  selector: 'app-form-task-group',
  standalone: true,
  imports: [
    CommonModule,FormsModule, ReactiveFormsModule
  ],
  templateUrl:'./form-task-group.component.html',
  styleUrls: ['./form-task-group.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormTaskGroupComponent implements OnInit{
  @Input()
  task!:Task;


  @Output()
  taskEmmiter:EventEmitter<Task> = new EventEmitter<Task>();

  form!: FormGroup;

  constructor(private taskService: TaskService,
    private formBuilder: FormBuilder,
    private router: Router){  }

  ngOnInit(): void {
    // crear un grupo
    this.form = this.formBuilder.group({
      // campos del formulario (campos que se mapean al al formulairo del html)
      // nombre del campo (mapeado con formControlName del html) : [valores_iniciales,...validaciones]
      nameTask:[this.task.name,[Validators.required,Validators.minLength(4)]], // no se le da valor inicial, ni validaciones
      completedTask:[this.task.completed,null],
      detailTask:[this.task?.detail,null]
    });
  }

  onSubmitForm():void{
    console.log('onSubmit');

    let taskAux:Task ={
      id: this.task.id,
      name:this.form.get('nameTask')?.value,
      completed:this.form.get('completedTask')?.value,
      detail:this.form.get('detailTask')?.value

    }
    this.taskService.addTask(taskAux);
    this.taskEmmiter.emit(taskAux);

    this.router.navigate(['/task']);
  }
 }
