import { Component, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Task } from 'src/app/models/task/task.model';

import { TaskService } from 'src/app/services/task.service';


@Component({
    selector: 'app-task-detail',
    templateUrl: 'task-detail.component.html',
    styleUrls: ['./task-detail.component.css'],
})
export class TaskDetailComponent {

  taskId!:number;
  task!:Task;
  showForm:boolean = false;

  constructor(private taskService:TaskService,private route: ActivatedRoute, private router: Router){}

  ngOnInit() {
    this.taskId = Number(this.route.snapshot.paramMap.get('id'));
    let taskAux:Task|undefined=this.taskService.getTask(this.taskId);
    if(taskAux){
      this.task = taskAux;
    }else{
      this.router.navigate(['/task']);
    }


 }

 isEnd():boolean{
    return this.task?.completed?this.task.completed:false;
 }

 modificate(){
  console.log("editar")
  this.showForm=true;
 }

 saveChange(event:Task):void{
  console.log('onSubmit');
    this.showForm=false;
  }

}
