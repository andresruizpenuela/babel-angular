
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Task } from '../../models/task/task.model';

@Component({
    selector: 'app-form-task',
    templateUrl: 'form-task.component.html',
    styleUrls: ['./form-task.component.css'],

})
export class FormTaskComponent {

  @Input()
  task!:Task;


  @Output()
  taskEmmiter:EventEmitter<Task> = new EventEmitter<Task>();

  onSubmit():void{
    console.log('onSubmit');
    this.taskEmmiter.emit(this.task);
  }
}
