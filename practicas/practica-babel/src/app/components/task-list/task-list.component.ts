import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Task } from 'src/app/models/task/task.model';
import { TaskService } from '../../services/task.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-task-list',
    templateUrl: 'task-list.component.html',
    styleUrls: ['./task-list.component.css']
})
export class TaskListComponent {

  @Input()
  taskList!:Task[];

  @Input()
  taskNameFileds!:string[];


  constructor(private taskService:TaskService,private router:Router){}

  recoverTaskList = () => this.taskList=this.taskService.getAllTask();

  deleteTask(id:number){
    console.log("delete")
    this.taskService.removeTask(id);
    this.recoverTaskList();
  }

  editTask(task:Task){
    if(task.completed==false){
      this.router.navigate(['/detail/'+task.id]);
    }
  }
}
